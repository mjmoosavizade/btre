To run this project first, you need to set up a virtual environment.

## Set up venv


```bash
python -m venv ./venv
```
and then install the requirements.

## Requirements

```bash
pip install -r requirements.txt

```
and then load statics

## Collect statics

```bash
python manage.py collectstatic

```